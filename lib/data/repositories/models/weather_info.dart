

import '../../network_services/models/response/city.dart';

class WeatherInfo {
  int? id;
  String? temp;
  String? feelsLike;
  String tempMin;
  String tempMax;
  String? conditions;
  City? city;
  DateTime dateTime;

  WeatherInfo(
      {this.id,
        this.temp,
        required this.tempMax,
        required this.tempMin,
        this.feelsLike,
        this.conditions,
        this.city,
        required this.dateTime}
      );
}
