
import 'package:weather/utils/datetime.dart';

import '../network_services/models/response/current_weather_response.dart';
import '../network_services/models/response/weather_forecast_response.dart';
import '../network_services/weather_service.dart';
import 'models/weather_info.dart';

class WeatherRepository {

  final WeatherService weatherService;

  WeatherRepository({required this.weatherService});

  Future<WeatherInfo> fetchCurrentWeatherForLocation(double lat, double lon) async {

    final currentWeatherResponse = await weatherService.fetchCurrentWeather(lat, lon);
    final currentWeatherObject = CurrentWeatherResponse.fromJson(currentWeatherResponse!);

    var weatherInfo = WeatherInfo(
        tempMax: currentWeatherObject.main!.tempMax!,
        temp: currentWeatherObject.main?.temp,
        tempMin: currentWeatherObject.main!.tempMin!,
        conditions: currentWeatherObject.weather?.first.main,
        dateTime: currentWeatherObject.dt != null ? dateFromTimeStamp(currentWeatherObject.dt!) : DateTime.now()
    );

    return  weatherInfo;
  }

  Future<List<WeatherInfo>> fetchWeatherForecastForLocation(double lat, double lon) async {
    final weatherForecastResponse = await weatherService.fetchWeatherForecast(lat, lon);
    final weatherForecastObject = WeatherForecastResponse.fromJson(weatherForecastResponse!);

    List<WeatherInfo> weatherInfoList = [];

    weatherForecastObject.list?.forEach((element) {

      weatherInfoList.add(
          new WeatherInfo(
            tempMax: element.main!.tempMax!,
            temp: element.main?.temp,
            tempMin: element.main!.tempMin!,
            conditions: element.weather?.first.main,
            city: weatherForecastObject.city,
            dateTime: element.dt != null ? dateFromTimeStamp(element.dt!) : DateTime.now()
          )
      );
    });

    return weatherInfoList;
  }

}