import 'city.dart';
import 'weather_data.dart';

class WeatherForecastResponse {
  String? cod;
  List<WeatherData>? list;
  City? city;

  WeatherForecastResponse(
      {this.cod, this.list, this.city});

  WeatherForecastResponse.fromJson(Map<String, dynamic> json) {
    cod = json['cod'];
    if (json['list'] != null) {
      list = <WeatherData>[];
      json['list'].forEach((v) {
        list!.add(new WeatherData.fromJson(v));
      });
    }
    city = json['city'] != null ? new City.fromJson(json['city']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['cod'] = this.cod;
    if (this.list != null) {
      data['list'] = this.list!.map((v) => v.toJson()).toList();
    }
    if (this.city != null) {
      data['city'] = this.city!.toJson();
    }
    return data;
  }
}
