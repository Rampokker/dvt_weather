
import 'dart:convert';

import 'package:http/http.dart';

class WeatherService {

  final weather_api_url = "api.openweathermap.org";
  final appid = "f65403cf84c7e1165458ebabaedfd11f";

  Future<Map<String, dynamic>?> fetchCurrentWeather(double lat, double lon) async {
    var queryParameters = {
      'lat': lat.toString(),
      'lon': lon.toString(),
      'appid': appid,
      'units': 'metric'
    };
    var uri = Uri.https(weather_api_url, 'data/2.5/weather', queryParameters);
    try {
      final response = await get(uri);
      return jsonDecode(response.body);
    } catch (e) {
      return null;
    }
  }

  Future<Map<String, dynamic>?> fetchWeatherForecast(double lat, double lon) async {
    var queryParameters = {
      'lat': lat.toString(),
      'lon': lon.toString(),
      'appid': appid,
      'units': 'metric'
    };
    var uri = Uri.https(weather_api_url, 'data/2.5/forecast', queryParameters);
    try {
      final response = await get(uri);
      return jsonDecode(response.body);
    } catch (e) {
      return null;
    }
  }

}
