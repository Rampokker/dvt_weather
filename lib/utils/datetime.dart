

import 'package:intl/intl.dart';

DateTime dateFromString(String sDate) {
  return DateTime.parse(sDate);
}

DateTime dateFromTimeStamp(int timestamp) {
  return DateTime.fromMillisecondsSinceEpoch(timestamp * 1000);
}

String dayOfWeekFromDate(DateTime date) {
  return DateFormat('EEEE').format(date);
}

String hoursMinutesFromDate(DateTime date) {
  return DateFormat('Hm').format(date);
}