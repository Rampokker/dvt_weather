
import 'package:flutter/material.dart';

const sunnyColor = Color(0xff47ab2f);
const cloudyColor = Color(0xff54717a);
const rainyColor = Color(0xff57575d);

const txtLargeWhite = TextStyle(
    fontWeight: FontWeight.bold,
    color: Colors.white,
    fontSize: 30.0
);

const txtMediumWhite = TextStyle(
    color: Colors.white,
    fontSize: 18.0
);

const txtSmallWhite = TextStyle(
    color: Colors.white,
    fontSize: 14.0
);