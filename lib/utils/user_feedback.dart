

import 'package:flutter/material.dart';
import 'package:weather/utils/theme.dart';

progressDialogShow(String sTitle){
  return Center(
    child: SizedBox(
      height: 80,
      child: Column(
          children: [
            const CircularProgressIndicator(),
            Container(margin: const EdgeInsets.only(top: 20),
                child:Text(sTitle, style: txtMediumWhite,)),
          ],
      ),
    ),
  );
}