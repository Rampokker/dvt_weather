import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import '../data/repositories/models/weather_info.dart';
import '../data/repositories/weather_repository.dart';

part 'weather_forecast_state.dart';

class WeatherForecastCubit extends Cubit<WeatherForecastState> {

  final WeatherRepository repository;

  WeatherForecastCubit({required this.repository}) : super(WeatherForecastInitial());

  void getWeatherForecastForLocation(double lat, double lon) {
    repository.fetchWeatherForecastForLocation(lat, lon).then((weatherInfoList) {
      emit(WeatherForecastLoaded(weatherInfoList));
    });
  }
}
