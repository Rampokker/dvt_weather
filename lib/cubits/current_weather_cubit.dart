import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';

import '../data/repositories/models/weather_info.dart';
import '../data/repositories/weather_repository.dart';

part 'current_weather_state.dart';

class WeatherCubit extends Cubit<WeatherState> {

  final WeatherRepository repository;

  WeatherCubit({required this.repository}) : super(WeatherInitial());

  void getCurrentWeatherForLocation(double lat, double lon) {
    repository.fetchCurrentWeatherForLocation(lat, lon).then((weatherInfo) {
      emit(WeatherLoaded(weatherInfo));
    });
  }
}
