part of 'geolocation_cubit.dart';

@immutable
abstract class GeolocationState {}

class GeolocationInitial extends GeolocationState {}

class GeolocationLoaded extends GeolocationState {
  final Position position;
  GeolocationLoaded(this.position);
}
