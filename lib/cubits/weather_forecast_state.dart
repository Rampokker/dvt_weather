part of 'weather_forecast_cubit.dart';


@immutable
abstract class WeatherForecastState {}

class WeatherForecastInitial extends WeatherForecastState {}

class WeatherForecastLoaded extends WeatherForecastState {
  final List<WeatherInfo> weatherForecastInfo;
  WeatherForecastLoaded(this.weatherForecastInfo);
}
