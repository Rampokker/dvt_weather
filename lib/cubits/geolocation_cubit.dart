import 'package:bloc/bloc.dart';
import 'package:geolocator/geolocator.dart';
import 'package:meta/meta.dart';

import '../data/repositories/Geolocation_repository.dart';
import 'current_weather_cubit.dart';


part 'geolocation_state.dart';

class GeolocationCubit extends Cubit<GeolocationState> {

  final GeoLocationRepository repository;
  final WeatherCubit weatherCubit;

  GeolocationCubit({required this.repository, required this.weatherCubit}) : super(GeolocationInitial());

  void determinePosition() {
    repository.determinePosition().then((position) {
      emit(GeolocationLoaded(position));

      weatherCubit.getCurrentWeatherForLocation(position.latitude, position.longitude);
    });
  }
}
