part of 'current_weather_cubit.dart';

@immutable
abstract class WeatherState {}

class WeatherInitial extends WeatherState {}

class WeatherLoaded extends WeatherState {
  final WeatherInfo currentWeatherInfo;
  WeatherLoaded(this.currentWeatherInfo);
}
