import 'package:flutter/material.dart';

import 'presentation/router.dart';

void main() {
  runApp(WeatherApp(
    router: AppRouter(),
  ));
}

class WeatherApp extends StatelessWidget {

  const WeatherApp({Key? key, required this.router}) : super(key: key);

  final AppRouter router;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      onGenerateRoute: router.generateRoute,
      debugShowCheckedModeBanner: false,
      // theme: CustomTheme.lightTheme,
      // home: TodoScreen(),
    );
  }
}