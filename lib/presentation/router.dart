import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:weather/cubits/geolocation_cubit.dart';
import '../cubits/weather_forecast_cubit.dart';
import '../data/repositories/Geolocation_repository.dart';
import '../data/repositories/weather_repository.dart';
import '../cubits/current_weather_cubit.dart';
import '../data/network_services/weather_service.dart';
import 'screens/current_weather_screen.dart';


class AppRouter {

  late WeatherRepository weatherRepository;
  late GeoLocationRepository geoLocationRepository;
  late WeatherCubit weatherCubit;
  late WeatherForecastCubit weatherForecastCubit;
  late GeolocationCubit geolocationCubit;

  AppRouter() {
    weatherRepository = WeatherRepository(weatherService: WeatherService());
    geoLocationRepository = GeoLocationRepository();
    weatherCubit = WeatherCubit(repository: weatherRepository);
    weatherForecastCubit = WeatherForecastCubit(repository: weatherRepository);
    geolocationCubit = GeolocationCubit(repository: geoLocationRepository, weatherCubit: weatherCubit);
  }

  Route generateRoute(RouteSettings routeSettings) {
    switch (routeSettings.name) {
      case "/":
        return MaterialPageRoute(
            builder: (_) =>
                MultiBlocProvider(
                  providers: [
                    BlocProvider<WeatherCubit>.value(
                      value: weatherCubit,
                    ),
                    BlocProvider<WeatherForecastCubit>.value(
                      value: weatherForecastCubit,
                    ),
                    BlocProvider<GeolocationCubit>.value(
                      value: geolocationCubit,
                    ),
                  ],  child: WeatherScreen(),
                )
        );
      // case EDIT_TODO_ROUTE:
      //   return MaterialPageRoute(builder: (_) => EditTodoScreen());
      default:
        return MaterialPageRoute(
          builder: (_) => BlocProvider.value(
            value: weatherCubit,
            child: WeatherScreen(),
          ),
        );
    }
  }
}