import 'package:flutter/material.dart';

import '../../data/repositories/models/weather_info.dart';
import '../../utils/constants.dart';
import '../../utils/datetime.dart';
import '../../utils/theme.dart';

class WeatherForecastWidget extends StatelessWidget {

  Widget iconForConditions(String conditions) {
    switch(conditions) {
      case "Clear": {  return Image.asset("assets/icons/clear.png"); }

      case "Rain": {  return Image.asset("assets/icons/rain.png"); }

      case "Clouds": {  return Image.asset("assets/icons/partlysunny.png"); }

      default: { return Image.asset("assets/icons/clear.png"); }
    }
  }

  List<WeatherInfo> weatherInfoList;
  WeatherForecastWidget({Key? key, required this.weatherInfoList}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(20.0),
      child: ListView.builder(
          itemCount: weatherInfoList.length,
          itemBuilder: (BuildContext context, int index) {
            return forecastListItem(weatherInfoList[index]);
          }),
    );
  }

  Widget forecastListItem(WeatherInfo weatherInfo) {
    return SizedBox(
      height: 50,
      child: Row(
          children: [
            Expanded(
              flex: 2,
              child: Text(
                dayOfWeekFromDate(weatherInfo.dateTime), style: txtMediumWhite,),
            ),
            Expanded(
              flex: 2,
              child: iconForConditions(weatherInfo.conditions!)
            ),
            Expanded(
              flex: 3,
              child: Text("${weatherInfo.tempMin}$tempSuffix  -  ${weatherInfo.tempMax}$tempSuffix",
                style: txtMediumWhite,
                textAlign: TextAlign.right,),
            ),
          ]
      ),
    );
  }
}
