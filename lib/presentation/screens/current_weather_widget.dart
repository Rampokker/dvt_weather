import 'package:flutter/material.dart';
import 'package:weather/utils/datetime.dart';

import '../../data/repositories/models/weather_info.dart';
import '../../utils/constants.dart';
import '../../utils/theme.dart';

class CurrentWeatherWidget extends StatelessWidget {

  WeatherInfo weatherInfo;
  CurrentWeatherWidget({Key? key, required this.weatherInfo}) : super(key: key);

  String getBackgroundImageForWeather(String conditions) {
    switch(conditions) {
      case "Clear": {  return "assets/images/forest_sunny.png"; }

      case "Rain": {  return "assets/images/forest_rainy.png"; }

      case "Clouds": {  return "assets/images/forest_cloudy.png"; }

      default: { return "assets/images/forest_cloudy.png"; }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Column(
        children: [
          Expanded(
            flex: 1,
            child: Stack(
              children: [

                Image.asset(getBackgroundImageForWeather(weatherInfo.conditions!),
                  fit: BoxFit.cover,
                  height: double.infinity,
                  width: double.infinity,
                  alignment: Alignment.center,
                ),

                Center(
                  child: Text("${weatherInfo.temp}$tempSuffix",
                    style: txtLargeWhite,),
                ),

                Center(
                  child: Padding(
                    padding: const EdgeInsets.only(top: 60.0),
                    child: Text("${weatherInfo.conditions}",
                      style: txtMediumWhite,
                      textAlign: TextAlign.center,),
                  ),
                ),

                Positioned(
                    top: 40,
                    left: 20,
                    child: Text("${weatherInfo.city?.name}\n${weatherInfo.city?.country}",
                      style: txtMediumWhite,)
                ),

                // Positioned(
                //     top: 40,
                //     right: 20,
                //     child: Text("Lat: ${weatherInfo.city?.coord?.lat.toString()}\nLon: ${weatherInfo.city?.coord?.lon.toString()}",
                //       style: txtMediumWhite,)
                // ),

                Positioned(
                    bottom: 20,
                    left: 20,
                    child: Text("Sunrise: ${hoursMinutesFromDate(dateFromTimeStamp(weatherInfo.city!.sunrise!))}",
                      style: txtMediumWhite,)
                ),

                Positioned(
                    bottom: 20,
                    right: 20,
                    child: Text("Sunset: ${hoursMinutesFromDate(dateFromTimeStamp(weatherInfo.city!.sunset!))}",
                      style: txtMediumWhite,)
                ),
              ],
            ),
          ),


          SizedBox(
            height: 50,
            child: Row(
              children: [
                Expanded(
                    flex: 1,
                    child: ListTile(
                      title: Text("${weatherInfo.tempMin}$tempSuffix",
                        style: txtMediumWhite,),
                      subtitle: const Text(
                        "Min", style: txtSmallWhite,),
                    )
                ),
                Expanded(
                    flex: 1,
                    child: ListTile(
                      title: Text("${weatherInfo.temp}$tempSuffix",
                        style: txtMediumWhite,
                        textAlign: TextAlign.center,),
                      subtitle: const Text("Current",
                        textAlign: TextAlign.center,
                        style: txtSmallWhite,),
                    )
                ),
                Expanded(
                    flex: 1,
                    child: ListTile(
                      title: Text("${weatherInfo.tempMax}$tempSuffix",
                        style: txtMediumWhite,
                        textAlign: TextAlign.right,),
                      subtitle: const Text("Max", textAlign: TextAlign
                          .right, style: txtSmallWhite,),
                    )
                ),
              ],
            ),
          )
        ]
    );
  }
}
