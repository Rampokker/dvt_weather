
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:geolocator/geolocator.dart';
import 'package:weather/cubits/geolocation_cubit.dart';
import 'package:weather/cubits/weather_forecast_cubit.dart';
import 'package:weather/presentation/screens/weather_forecast_widget.dart';

import '../../cubits/current_weather_cubit.dart';
import '../../utils/theme.dart';
import '../../utils/user_feedback.dart';
import '../bl/convert_weather_list.dart';
import 'current_weather_widget.dart';


class WeatherScreen extends StatelessWidget {
  const WeatherScreen({Key? key}) : super(key: key);

  Color getBackgroundColorForWeather(String conditions) {
    switch(conditions) {
      case "Clear": {  return sunnyColor; }

      case "Rain": {  return rainyColor; }

      case "Clouds": {  return cloudyColor; }

      default: { return cloudyColor; }
    }
  }

  @override
  Widget build(BuildContext context) {
    late Position location;

    BlocProvider.of<GeolocationCubit>(context).determinePosition();

    return BlocBuilder<GeolocationCubit, GeolocationState>(
      builder: (context, state) {
        if (state is! GeolocationLoaded) {
          return Scaffold(
            backgroundColor: sunnyColor,
            body: progressDialogShow("Determining your location..."),
          );
        }

        location = state.position;
        BlocProvider.of<WeatherCubit>(context).getCurrentWeatherForLocation(
            location.latitude, location.longitude);
        BlocProvider.of<WeatherForecastCubit>(context)
            .getWeatherForecastForLocation(
            location.latitude, location.longitude);

        return BlocBuilder<WeatherCubit, WeatherState>(
            builder: (context, state) {
              if (state is! WeatherLoaded) {
                return Scaffold(
                  backgroundColor: sunnyColor,
                  body: progressDialogShow("Loading current weather..."),
                );
              }

              final currentWeatherInfo = (state).currentWeatherInfo;

              return Scaffold(
                backgroundColor: getBackgroundColorForWeather(currentWeatherInfo.conditions!),
                body: Column(
                  children: [

                    Expanded(
                        flex: 1,
                        child: BlocBuilder<WeatherForecastCubit,
                            WeatherForecastState>(
                            builder: (context, state) {
                              if (state is! WeatherForecastLoaded) {
                                return progressDialogShow(
                                    "Loading weather forecast...");
                              }
                              final weatherInfoList = (state).weatherForecastInfo;
                              var weatherInfoToday = weatherInfoList.first;

                              weatherInfoToday.conditions = currentWeatherInfo.conditions!;
                              weatherInfoToday.temp = currentWeatherInfo.temp!;

                              return CurrentWeatherWidget(
                                weatherInfo: weatherInfoToday,);
                            }
                        )
                    ),

                    Expanded(
                        flex: 1,
                        child: BlocBuilder<WeatherForecastCubit,
                            WeatherForecastState>(
                            builder: (context, state) {
                              if (state is! WeatherForecastLoaded) {
                                return progressDialogShow(
                                    "Loading weather forecast...");
                              }
                              final weatherInfoList = (state).weatherForecastInfo;
                              return WeatherForecastWidget(
                                weatherInfoList: convertForecastListToDaily(weatherInfoList),);
                            }
                        )
                    ),

                  ],
                ),
              );
            }
        );
      },
    );
  }

}





