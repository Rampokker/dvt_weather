

import '../../data/repositories/models/weather_info.dart';
import '../../utils/datetime.dart';

List<WeatherInfo> convertForecastListToDaily(List<WeatherInfo> weatherInfoList) {

  Map<String, List<WeatherInfo>> dailyList = {};
  List<WeatherInfo> dailyListDistinct = [];

  for (var weatherInfo in weatherInfoList) {
    var currentDay = dayOfWeekFromDate(weatherInfo.dateTime);
      dailyList.putIfAbsent(currentDay, () => []);
      dailyList[currentDay]?.add(weatherInfo);
  }

  dailyList.forEach((key, value) {
    var weatherForDay = dailyList[key];
    var weatherForDayAverage = weatherForDay?.first;

    weatherForDay?.forEach((element) {
      if (double.parse(element.tempMin) < double.parse(weatherForDayAverage!.tempMin)) {
        weatherForDayAverage.tempMin = element.tempMin;
      }
      if (double.parse(element.tempMax) > double.parse(weatherForDayAverage.tempMax)) {
        weatherForDayAverage.tempMax = element.tempMax;
      }
    });

    dailyListDistinct.add(weatherForDayAverage!);
  });

  return dailyListDistinct;
}
